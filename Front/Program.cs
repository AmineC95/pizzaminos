var builder = WebApplication.CreateBuilder(args);

// Add environment-specific configuration files
builder.Configuration
    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
    .AddJsonFile($"appsettings.{builder.Environment.EnvironmentName}.json", optional: true, reloadOnChange: true);

var app = builder.Build();

// Serve the static files in wwwroot (your built Quasar app will be here)
app.UseStaticFiles();

// In production, you'd want to serve the built Quasar app
if (app.Environment.IsProduction())
{
  app.UseSpa(spa =>
  {
    spa.Options.SourcePath = "wwwroot";
  });
}
else
{
  // In development, proxy the Quasar development server
  app.UseSpa(spa =>
  {
    spa.Options.SourcePath = "ClientApp";
    spa.UseProxyToSpaDevelopmentServer(builder.Configuration["Frontend:DevServerUrl"]);
  });
}

app.Run();
