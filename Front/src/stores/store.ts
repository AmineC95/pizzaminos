import { reactive } from "vue";

interface Pizza {
  id: number;
  name: string;
  description: string;
  price: number;
  image?: string;
}

interface Store {
  cart: Pizza[];
  addToCart: (pizza: Pizza) => void;
  removeFromCart: (id: number) => void;
}

const store: Store = reactive({
  cart: [],
  addToCart: function (pizza: Pizza) {
    this.cart.push(pizza);
  },
  removeFromCart: function (id: number) {
    const index = this.cart.findIndex((p) => p.id === id);
    if (index !== -1) {
      this.cart.splice(index, 1);
    }
  },
});

export default store;
