/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface CreditCard {
  /**
   * @format credit-card
   * @minLength 1
   */
  cardNumber: string;
  /** @minLength 1 */
  cardHolder: string;
  /**
   * @minLength 1
   * @pattern ^(0[1-9]|1[0-2])/?([0-9]{4}|[0-9]{2})$
   */
  expiryDate: string;
  /**
   * @minLength 1
   * @pattern ^\d{3}$
   */
  cvv: string;
}
