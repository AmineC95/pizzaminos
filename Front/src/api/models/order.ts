/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface Order {
  /** @format int32 */
  id?: number;
  /** @format int32 */
  pizzaId?: number;
  toppings?: Topping[] | null;
  /** @format double */
  amount?: number;
  orderToppings?: OrderTopping[] | null;
}

export interface OrderCreateDto {
  /** @format int32 */
  pizzaId?: number;
  /** @format double */
  amount?: number;
  toppings?: OrderToppingCreateDto[] | null;
}

export interface OrderReadDto {
  /** @format int32 */
  id?: number;
  /** @format int32 */
  pizzaId?: number;
  /** @format double */
  amount?: number;
  toppings?: ToppingReadDto[] | null;
}

export interface OrderTopping {
  /** @format int32 */
  orderId?: number;
  /** @format int32 */
  toppingId?: number;
  /** @format int32 */
  quantity?: number;
  order?: Order;
  topping?: Topping;
}

export interface OrderToppingCreateDto {
  /** @format int32 */
  toppingId?: number;
  /** @format int32 */
  quantity?: number;
}

export interface Pizza {
  /** @format int32 */
  id?: number;
  name?: string | null;
  description?: string | null;
  /** @format double */
  price?: number;
}

export interface Topping {
  /** @format int32 */
  id?: number;
  name?: string | null;
  /** @format double */
  price?: number;
  orderToppings?: OrderTopping[] | null;
}

export interface ToppingCreateDto {
  name?: string | null;
  /** @format double */
  price?: number;
}

export interface ToppingReadDto {
  /** @format int32 */
  id?: number;
  name?: string | null;
  /** @format double */
  price?: number;
}

export interface ToppingUpdateDto {
  name?: string | null;
  /** @format double */
  price?: number;
}
