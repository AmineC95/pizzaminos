import axios from "axios";
import { Pizza } from "../models/menu";

const apiMenu = axios.create({
  baseURL: "http://3.67.16.105/",
});

export const getPizzas = async (): Promise<Pizza[]> => {
  const response = await apiMenu.get<Pizza[]>("api/Pizza");
  return response.data;
};

export const getPizzaById = async (id: number): Promise<Pizza> => {
  const response = await apiMenu.get<Pizza>(`/pizza/${id}`);
  return response.data;
};

export const createPizza = async (pizza: Pizza): Promise<Pizza> => {
  const response = await apiMenu.post<Pizza>("/pizza", pizza);
  return response.data;
};

export default apiMenu;
