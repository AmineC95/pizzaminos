import axios from "axios";
import {
  Order,
  OrderCreateDto,
  Topping,
  ToppingCreateDto,
  ToppingUpdateDto,
} from "../models/order";

const apiOrder = axios.create({
  baseURL: "http://18.158.48.124/",
});

// Order APIs
export const getAllOrders = async (): Promise<Order[]> => {
  const response = await apiOrder.get<Order[]>("api/Order");
  return response.data;
};

export const getOrderById = async (id: number): Promise<Order> => {
  const response = await apiOrder.get<Order>(`api/Order/${id}`);
  return response.data;
};

export const createOrder = async (order: OrderCreateDto): Promise<Order> => {
  const response = await apiOrder.post<Order>("api/Order", order);
  return response.data;
};

export const updateOrder = async (
  id: number,
  order: OrderCreateDto
): Promise<void> => {
  await apiOrder.put(`api/Order/${id}`, order);
};

export const deleteOrder = async (id: number): Promise<void> => {
  await apiOrder.delete(`api/Order/${id}`);
};

// Topping APIs
export const getAllToppings = async (): Promise<Topping[]> => {
  const response = await apiOrder.get<Topping[]>("api/Toppings");
  return response.data;
};

export const getToppingById = async (id: number): Promise<Topping> => {
  const response = await apiOrder.get<Topping>(`/toppings/${id}`);
  return response.data;
};

export const createTopping = async (
  topping: ToppingCreateDto
): Promise<Topping> => {
  const response = await apiOrder.post<Topping>("/toppings", topping);
  return response.data;
};

export const updateTopping = async (
  id: number,
  topping: ToppingUpdateDto
): Promise<void> => {
  await apiOrder.put(`/toppings/${id}`, topping);
};

export const deleteTopping = async (id: number): Promise<void> => {
  await apiOrder.delete(`/toppings/${id}`);
};

export default apiOrder;
