import axios from "axios";
import { CreditCard } from "../models/payment";

const apiPayment = axios.create({
  baseURL: "http://3.72.200.203/", // URL de base pour le microservice menu
});

export const getAllPayments = async (): Promise<CreditCard[]> => {
  const response = await apiPayment.get<CreditCard[]>("api/CreditCard");
  return response.data;
};

export const createPayment = async (
  payment: CreditCard
): Promise<CreditCard> => {
  const response = await apiPayment.post<CreditCard>("/payment", payment);
  return response.data;
};

export default apiPayment;
