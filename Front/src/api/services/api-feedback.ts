import axios from "axios";
import { Feedback } from "../models/feedback";

const apiFeedback = axios.create({
  baseURL: "http://3.127.129.76/",
});

export const getAllFeedbacks = async (): Promise<Feedback[]> => {
  const response = await apiFeedback.get<Feedback[]>("api/Feedback");
  return response.data;
};

export const getFeedbackById = async (id: number): Promise<Feedback> => {
  const response = await apiFeedback.get<Feedback>(`/Feedback/${id}`);
  return response.data;
};

export const createFeedback = async (feedback: Feedback): Promise<Feedback> => {
  const response = await apiFeedback.post<Feedback>("/Feedback", feedback);
  return response.data;
};

export const deleteFeedback = async (id: number): Promise<void> => {
  await apiFeedback.delete(`/Feedback/${id}`);
};

export default apiFeedback;
