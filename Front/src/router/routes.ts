import { RouteRecordRaw } from "vue-router";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/IndexPage.vue") },
      { path: "menu", component: () => import("pages/Menu.vue") },
      { path: "cart", component: () => import("pages/Cart.vue") },
      { path: "login", component: () => import("pages/Login.vue") },
      { path: "register", component: () => import("pages/Register.vue") },
      { path: "account", component: () => import("pages/Account.vue") },
      { path: "orders", component: () => import("pages/Orders.vue") },
      { path: "payment", component: () => import("pages/Payment.vue") },
    ],
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
