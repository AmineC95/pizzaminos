﻿using System.ComponentModel.DataAnnotations;

namespace API_Payment.Models
{
    public class CreditCard
    {
        [Required]
        [CreditCard]
        public string CardNumber { get; set; }

        [Required]
        public string CardHolder { get; set; }

        [Required]
        [RegularExpression("^(0[1-9]|1[0-2])/?([0-9]{4}|[0-9]{2})$", ErrorMessage = "Invalid Expiry Date")]
        public string ExpiryDate { get; set; }

        [Required]
        [RegularExpression(@"^\d{3}$", ErrorMessage = "Invalid CVV")]
        public string CVV { get; set; }
    }
}
