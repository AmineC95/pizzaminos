﻿namespace API_Payment.Models
{
    public class Transaction
    {
        public int TransactionId { get; set; }
        public decimal Amount { get; set; }
        public string Status { get; set; }
        public string CardNumber { get; set; }
    }
}
