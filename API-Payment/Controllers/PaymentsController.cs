﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using API_Payment.Data;
using API_Payment.Models;

namespace API_Payment.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    //[Authorize]
    public class PaymentsController : ControllerBase
    {
        private readonly PaymentDbContext _context;

        public PaymentsController(PaymentDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        //[Authorize]
        public async Task<IActionResult> MakePayment([FromBody] CreditCard paymentDetails)
        {
            // Validez les détails de la carte
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Logique pour simuler le paiement
            var transaction = new Transaction
            {
                Amount = 100,
                CardNumber = paymentDetails.CardNumber,
                Status = "Successful"
            };

            _context.Transactions.Add(transaction);
            await _context.SaveChangesAsync();

            return Ok(transaction);
        }
    }
}
