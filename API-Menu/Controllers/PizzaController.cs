using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using API_Menu.Data;
using API_Menu;
using Microsoft.AspNetCore.Authorization;

namespace API_Menu.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    //[Authorize]
    public class PizzaController : ControllerBase
    {
        private readonly PizzaContext _context;

        public PizzaController(PizzaContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Pizza>> GetAll()
        {
            return _context.Pizzas.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Pizza> GetById(int id)
        {
            var pizza = _context.Pizzas.Find(id);
            if (pizza == null)
            {
                return NotFound();
            }
            return pizza;
        }

        [HttpPost]
        public ActionResult<Pizza> Create(Pizza pizza)
        {
            _context.Pizzas.Add(pizza);
            _context.SaveChanges();

            return CreatedAtAction(nameof(GetById), new { id = pizza.Id }, pizza);
        }
    }
}
