﻿using Microsoft.EntityFrameworkCore;

namespace API_Menu.Data
{
    public class PizzaContext : DbContext
    {
        public DbSet<Pizza> Pizzas { get; set; }

        public PizzaContext(DbContextOptions<PizzaContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Configure les propriétés et relations des entités
            modelBuilder.Entity<Pizza>()
                .HasKey(p => p.Id); // Définit la clé primaire
            // Autres configurations si nécessaire
        }
    }
}
