using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using API_Menu.Data;
using Microsoft.OpenApi.Models;
using System;
using Microsoft.AspNetCore.Authorization;

namespace API_Menu
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureAppConfiguration((hostingContext, config) =>
                    {
                        config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                    })
                    .ConfigureServices((hostingContext, services) =>
                    {
                        services.AddCors(options =>
                        {
                            options.AddDefaultPolicy(builder =>
                            {
                                builder.WithOrigins(
                                    "http://pizzaminosawsfront.s3-website.eu-central-1.amazonaws.com",
                                    "http://d355xu5yg84l7p.cloudfront.net",
                                    "http://localhost:9000"
                                    ) 
                                       .AllowAnyHeader()
                                       .AllowAnyMethod();
                            });
                        });

                        services.AddDbContext<PizzaContext>(options =>
                            options.UseMySql(hostingContext.Configuration.GetConnectionString("DefaultConnection"), new MySqlServerVersion(new Version(8, 0, 21))));

                        //services.AddAuthentication(options =>
                        //{
                        //    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        //    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                        //})
                        //.AddJwtBearer(options =>
                        //{
                        //    options.Authority = "https://YOUR_AUTHORITY/";
                        //    options.Audience = "YOUR_AUDIENCE";
                        //});

                        services.AddSwaggerGen(c =>
                        {
                            c.SwaggerDoc("v1", new OpenApiInfo { Title = "API Menu", Version = "v1" });
                        });

                        services.AddControllers();
                    })
                    .Configure((hostingContext, app) =>
                    {
                        app.UseSwagger();
                        app.UseSwaggerUI(c =>
                        {
                            c.SwaggerEndpoint("/swagger/v1/swagger.json", "API Menu V1");
                            c.RoutePrefix = "swagger";
                        });

                        app.UseCors();

                        app.UseRouting();
                        //app.UseAuthentication(); 
                        app.UseAuthorization(); 

                        app.UseEndpoints(endpoints =>
                        {
                            endpoints.MapControllers();
                        });
                    });
                });
    }
}
