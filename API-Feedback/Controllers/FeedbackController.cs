using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using API_Feedback;
using API_Feedback.Data;
using Microsoft.AspNetCore.Authorization;

namespace API_Feedback.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class FeedbackController : ControllerBase
    {
        private readonly FeedbackContext _context;

        public FeedbackController(FeedbackContext context)
        {
            _context = context;
        }

        // GET: api/Feedback
        [HttpGet]
        public ActionResult<IEnumerable<Feedback>> GetFeedbacks()
        {
            return _context.Feedback.ToList();
        }

        // GET: api/Feedback/5
        [HttpGet("{id}")]
        public ActionResult<Feedback> GetFeedback(int id)
        {
            var feedback = _context.Feedback.Find(id);

            if (feedback == null)
            {
                return NotFound();
            }

            return feedback;
        }

        // POST: api/Feedback
        [HttpPost]
        public ActionResult<Feedback> PostFeedback(Feedback feedback)
        {
            _context.Feedback.Add(feedback);
            _context.SaveChanges();

            return CreatedAtAction(nameof(GetFeedback), new { id = feedback.Id }, feedback);
        }

        // DELETE: api/Feedback/5
        [HttpDelete("{id}")]
        public IActionResult DeleteFeedback(int id)
        {
            var feedback = _context.Feedback.Find(id);
            if (feedback == null)
            {
                return NotFound();
            }

            _context.Feedback.Remove(feedback);
            _context.SaveChanges();

            return NoContent();
        }
    }
}
