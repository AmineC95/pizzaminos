namespace API_Feedback
{
    public class Feedback
    {
        public int Id { get; set; }
        public int UserId { get; set; }  // Identifiant utilisateur fourni par OpenID
        public string Comment { get; set; } // Commentaire de l'utilisateur
        public int Rating { get; set; }     // Note donn�e par l'utilisateur, par exemple sur une �chelle de 1 � 5
    }
}
