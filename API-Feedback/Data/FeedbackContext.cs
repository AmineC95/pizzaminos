﻿using Microsoft.EntityFrameworkCore;
using API_Feedback;

namespace API_Feedback.Data
{
    public class FeedbackContext : DbContext
    {
        public DbSet<Feedback> Feedback { get; set; }

        public FeedbackContext(DbContextOptions<FeedbackContext> options) : base(options)
        {
        }
    }
}
