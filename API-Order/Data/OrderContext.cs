﻿using Microsoft.EntityFrameworkCore;
using API_Order.Models;

namespace API_Order.Data
{
    public class OrderContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<Topping> Toppings { get; set; }
        public DbSet<OrderTopping> OrderToppings { get; set; }

        public OrderContext(DbContextOptions<OrderContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderTopping>()
                .HasKey(ot => new { ot.OrderId, ot.ToppingId });

            modelBuilder.Entity<OrderTopping>()
                .HasOne(ot => ot.Order)
                .WithMany(o => o.OrderToppings)
                .HasForeignKey(ot => ot.OrderId);

            modelBuilder.Entity<OrderTopping>()
                .HasOne(ot => ot.Topping)
                .WithMany(t => t.OrderToppings)
                .HasForeignKey(ot => ot.ToppingId);

            // Configurez d'autres éléments de votre modèle

            base.OnModelCreating(modelBuilder);
        }
    }
}
