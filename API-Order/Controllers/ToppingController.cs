﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_Order.Models;
using API_Order.Data;
using API_Order.Dtos;
[Route("api/[controller]")]
[ApiController]
public class ToppingsController : ControllerBase
{
    private readonly OrderContext _context;

    public ToppingsController(OrderContext context)
    {
        _context = context;
    }

    // GET: api/Toppings
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Topping>>> GetToppings()
    {
        return await _context.Toppings.ToListAsync();
    }

    // GET: api/Toppings/5
    [HttpGet("{id}")]
    public async Task<ActionResult<Topping>> GetTopping(int id)
    {
        var topping = await _context.Toppings
            .Include(t => t.OrderToppings)
            .ThenInclude(ot => ot.Order)
            .FirstOrDefaultAsync(t => t.Id == id);

        if (topping == null)
        {
            return NotFound();
        }

        return topping;
    }

    [HttpPost]
    public async Task<ActionResult<ToppingReadDto>> PostTopping(ToppingCreateDto toppingDto)
    {
        var topping = new Topping
        {
            Name = toppingDto.Name,
            Price = toppingDto.Price,
        };

        _context.Toppings.Add(topping);
        await _context.SaveChangesAsync();

        var toppingReadDto = new ToppingReadDto
        {
            Id = topping.Id,
            Name = topping.Name,
            Price = topping.Price
        };

        return CreatedAtAction("GetTopping", new { id = topping.Id }, toppingReadDto);
    }


    [HttpPut("{id}")]
    public async Task<IActionResult> PutTopping(int id, ToppingUpdateDto toppingDto)
    {
        var topping = await _context.Toppings.FindAsync(id);
        if (topping == null)
        {
            return NotFound();
        }

        topping.Name = toppingDto.Name;
        topping.Price = toppingDto.Price;

        try
        {
            await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!ToppingExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }

        return NoContent();
    }



    // DELETE: api/Toppings/5
    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteTopping(int id)
    {
        var topping = await _context.Toppings.FindAsync(id);
        if (topping == null)
        {
            return NotFound();
        }

        _context.Toppings.Remove(topping);
        await _context.SaveChangesAsync();

        return NoContent();
    }

    private bool ToppingExists(int id)
    {
        return _context.Toppings.Any(e => e.Id == id);
    }
}
