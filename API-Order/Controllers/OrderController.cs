﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using API_Order.Data;
using API_Order.Models;
using Microsoft.AspNetCore.Authorization;
using API_Order.Dtos;
using Microsoft.EntityFrameworkCore;

namespace API_Order.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    //[Authorize]
    public class OrderController : ControllerBase
    {
        private readonly OrderContext _context;
        private readonly IHttpClientFactory _httpClientFactory;

        public OrderController(OrderContext context, IHttpClientFactory httpClientFactory)
        {
            _context = context;
            _httpClientFactory = httpClientFactory;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Order>> GetOrders()
        {
            return _context.Orders.ToList();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<OrderReadDto>> GetOrder(int id)
        {
            var order = await _context.Orders
                .Include(o => o.OrderToppings)
                .ThenInclude(ot => ot.Topping)
                .FirstOrDefaultAsync(o => o.Id == id);

            if (order == null)
            {
                return NotFound();
            }

            var orderReadDto = new OrderReadDto
            {
                Id = order.Id,
                PizzaId = order.PizzaId,
                Amount = order.Amount,
                Toppings = order.OrderToppings.Select(ot => new ToppingReadDto
                {
                    Id = ot.Topping.Id,
                    Name = ot.Topping.Name,
                    Price = ot.Topping.Price
                }).ToList()
            };

            return orderReadDto;
        }

        [HttpPost]
        public async Task<ActionResult<OrderReadDto>> PostOrder(OrderCreateDto orderDto)
        {
            var order = new Order { PizzaId = orderDto.PizzaId, Amount = orderDto.Amount };

            foreach (var orderToppingDto in orderDto.Toppings)
            {
                var topping = await _context.Toppings.FindAsync(orderToppingDto.ToppingId);

                if (topping == null)
                {
                    return NotFound();
                }

                var orderTopping = new OrderTopping
                {
                    Order = order,
                    Topping = topping,
                    Quantity = orderToppingDto.Quantity
                };

                _context.OrderToppings.Add(orderTopping);
            }

            await _context.SaveChangesAsync();

            // convert Order to OrderReadDto
            var orderReadDto = new OrderReadDto
            {
                Id = order.Id,
                PizzaId = order.PizzaId,
                Amount = order.Amount,
                Toppings = order.OrderToppings.Select(ot => new ToppingReadDto
                {
                    Id = ot.Topping.Id,
                    Name = ot.Topping.Name,
                    Price = ot.Topping.Price
                }).ToList()
            };

            return CreatedAtAction("GetOrder", new { id = order.Id }, orderReadDto);
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteOrder(int id)
        {
            var order = _context.Orders.Find(id);
            if (order == null)
            {
                return NotFound();
            }

            _context.Orders.Remove(order);
            _context.SaveChanges();

            return NoContent();
        }
    }
}
