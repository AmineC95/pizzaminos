﻿namespace API_Order.Dtos
{
    public class ToppingCreateDto
    {
        public string Name { get; set; }
        public double Price { get; set; }
    }
}


