﻿namespace API_Order.Dtos
{
    public class OrderToppingCreateDto
    {
        public int ToppingId { get; set; }
        public int Quantity { get; set; }
    }
}
