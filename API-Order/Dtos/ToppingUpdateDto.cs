﻿namespace API_Order.Dtos
{
    public class ToppingUpdateDto
    {
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
