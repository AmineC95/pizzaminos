﻿namespace API_Order.Dtos
{
    public class OrderReadDto
    {
        public int Id { get; set; }
        public int PizzaId { get; set; }
        public double Amount { get; set; }
        public List<ToppingReadDto> Toppings { get; set; }
    }

}
