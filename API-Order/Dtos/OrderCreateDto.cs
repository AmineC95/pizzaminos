﻿using System.Collections.Generic;

namespace API_Order.Dtos
{
    public class OrderCreateDto
    {
        public int PizzaId { get; set; }
        public double Amount { get; set; }
        public List<OrderToppingCreateDto> Toppings { get; set; }
    }
}
