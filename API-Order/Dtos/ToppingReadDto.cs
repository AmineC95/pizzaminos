﻿namespace API_Order.Dtos
{
    public class ToppingReadDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
    }
}

