﻿using System.Collections.Generic;

namespace API_Order.Models
{
    public class Order
    {
        public int Id { get; set; }
        public int PizzaId { get; set; }
        public List<Topping> Toppings { get; set; }
        public double Amount { get; set; }

        public List<OrderTopping> OrderToppings { get; set; }
    }
}
