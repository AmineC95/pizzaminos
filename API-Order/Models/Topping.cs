﻿namespace API_Order.Models
{
    public class Topping
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        public List<OrderTopping> OrderToppings { get; set; }
    }
}
