﻿namespace API_Order.Models
{
    public class OrderTopping
    {
        public int OrderId { get; set; }
        public int ToppingId { get; set; }
        public int Quantity { get; set; }

        // Propriétés de navigation
        public Order Order { get; set; }
        public Topping Topping { get; set; }
    }
}
